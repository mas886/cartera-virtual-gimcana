<?php

include_once("./config.php");
generateConnection();

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

?>


<!doctype html>
<html lang="ca">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="./favicon.ico">

  <title>Marcador gimcana de Reis- Grup d'Esplai Apassomi</title>

  <link rel="canonical" href="https://apassomi.org/gimcana/">

  <!-- Bootstrap core CSS -->
  <link href="./dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="./custom.css" rel="stylesheet">
</head>

<body>

  <div class="col-12 text-center">
    <p>S'han aconseguit un total de:</p>
    <span class="marcadorPunts">0</span>
    <span class="torrocoin">Torrocoins</span>
    <br /><br /><br />
    <h6 class="border-bottom border-gray pb-2 mb-0">Ranquing d'usuaris</h6>
    <div id="marcadorRes" class="my-3 p-3 bg-white rounded box-shadow puntsBox">
      <?php foreach (getMonstreAssignacions(1) as $assignacio) { ?>
        <div class="media text-muted pt-3">
          <div class="assBox"><?php echo $assignacio['quantitat'] ?></div>
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">
                <?php if ($assignacio['nom'] != "") {
                  echo $assignacio['nom'];
                } else {
                  echo "No nom visible";
                } ?></strong>
            </div>
            <span class="d-block">id. <?php echo $assignacio['usuari'] ?></span>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>

</body>

<script type="text/javascript" src="./dist/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="./custom.js"></script>

<script>
  var intervalId = setInterval(function() {
    getRanking();
  }, 20000);
</script>

</html>