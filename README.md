# Cartera Virtual Gimcana

Petita cartera virtual per a utilitzar en gimcanes. Utilitza una API Rest programada en PHP que es comunica amb el client mitjançant consultes en Ajax i paràmetres GET. 

L'index és el front end per als usuaris, i el /monitors.php és l'accés per a donar punts als usuaris. Els usuaris del backend están directament hardcodejats a l'API: 1MONSTKRAMP,2MONSTOLEN,3MONSTBEFA,4MONSTBELS.

Hi ha un marcador públic de punts amb un ranquing d'usuaris a /marcador.php

Els usuaris al seu torn estan a la base de dades, que ve preconfigurada amb 100 usuaris. L'aplicació és molt senzilla, i un breu cop d'ull al codi explicarà com funciona.

## Getting started

Servidor LAMP, pujar la base de dades, i configurar el config.php.


## License
Llicència GPLv2.
Arnau Mas

## Project status
Projecte tal qual està, no s'actualitzarà a no ser que es torne a necessitar.

