<?php

    session_start();

    define('dbMame',"gimcana");
    define('dbUser',"user");
    define('dbPass',"");
    define('dbHost',"localhost");

    function generateConnection() {
        //db connection
        $GLOBALS['$con'] = connect();
    
        //Don't do anything else if the db connection fails
        if (!$GLOBALS['$con']) {
            echo "Error connecting db.";
            exit;
        }
    }
    
    function connect() {
    
        $hostdb = dbHost;
        $userdb = dbUser;
        $passdb = dbPass;
        $database = dbMame;
    
        try {
            $connection = new PDO("mysql:host=$hostdb;dbname=$database;charset=utf8mb4", "$userdb", "$passdb", array(PDO::ATTR_PERSISTENT => true))or die(false);
        } catch (Exception $e) {
            $connection = false;
        }
        return $connection;
    }

    function getQuery($query, $variables = []) {
        $connection = $GLOBALS['$con'];
        $sql = $query;
        $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
        $sth = $connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $sth->execute($variables);
        $resultat = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $resultat;
    }
    
    function executeQuery($query, $variables = [], $getInsertId = false) {
        $connection = $GLOBALS['$con'];
        $sql = $query;
        $sth = $connection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        if ($getInsertId) {
            $sth->execute($variables);
            if ($connection->lastInsertId() == 0) {
                return false;
            } else {
                return $connection->lastInsertId();
            }
        } else {
            return $sth->execute($variables);
        }
    }


//////Consultes
function getUser($usuari){
    $userInfo= getQuery("SELECT `id`, `usuari`, `nom`, COALESCE((SELECT SUM(`quantitat`) FROM `torrons` WHERE `participantId`=`participants`.`id`),0) as punts, COALESCE((SELECT SUM(`quantitat`) FROM `torrons` WHERE 1),0) as total FROM `participants` WHERE usuari=:usuari;",[":usuari"=>$usuari]);
    return $userInfo;
}

function getUserMonstres($usuari){
    $monstres = getQuery("SELECT DISTINCT `monstreId` FROM `torrons` WHERE `participantId`=(SELECT `id` FROM `participants` WHERE `usuari`=:usuari)",[':usuari'=>$usuari]);
    $monstros=[];
    foreach($monstres as $monstro){
      array_push($monstros, $monstro['monstreId']);
    }
    return $monstros;
}

function getMonstreAssignacions($monstreId){
    $assignacions = getQuery("SELECT `id`, `monstreId`, (SELECT usuari FROM participants WHERE id=`torrons`.`participantId`) usuari, (SELECT nom FROM participants WHERE id=`torrons`.`participantId`) nom,`quantitat` FROM `torrons` WHERE monstreId = :monstreId ORDER BY id DESC;",[':monstreId'=>$monstreId]);
    return $assignacions;
}

function getTotals($monstreId){
    $totals = getQuery("SELECT COALESCE(SUM(`quantitat`),0) total, COALESCE((SELECT SUM(`quantitat`) FROM `torrons` WHERE `monstreId`=:monstreId)) as monstre FROM `torrons` WHERE 1;",[":monstreId"=>$monstreId]);
    return $totals;
}
