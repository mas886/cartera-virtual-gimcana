<?php

  include_once("./config.php");

  if(isset($_SESSION['moniUser'])){
    generateConnection();
    $totals=getTotals(substr($_SESSION['moniUser'], 0, 1))[0];
  }
?>


<!doctype html>
<html lang="ca">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="./favicon.ico">

  <title>Gimcana de Reis- Grup d'Esplai Apassomi</title>

  <link rel="canonical" href="https://apassomi.org/gimcana/">

  <!-- Bootstrap core CSS -->
  <link href="./dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="./custom.css" rel="stylesheet">
</head>

<body>
  <?php if(!isset($_SESSION['moniUser'])){ ?>
  <form id="pag0" class="form-signin" style="display: initial;">
    <div class="text-center mb-4">
      <h1 class="h3 mb-3 font-weight-normal">Accés monitors</h1>
      <p>Grup d'Esplai Apassomi</p>
    </div>

    <div class="form-label-group">
      <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
      <label for="inputEmail">Codi d'Entrada</label>
    </div>
    <button id="botoEntrarMonitors" class="btn btn-md btn-primary btn-block" type="submit">Entrar</button>
    <p class="mt-5 mb-3 text-muted text-center">Grup d'Esplai Apassomi</p>
  </form>

  <?php } else { ?>
    <div id="pag1" class="form-signin" >
      <div class="text-center mb-4">
        <h1 class="h3 mb-3 font-weight-normal user-title">Hola equip, </br><span id="moniGrup"><?php echo $_SESSION['moniUser'] ?></span></h1>
      </div>

      <div class="input-group mb-4">

        <input type="text" id="grupDefinir" class="form-control" placeholder="A quin grup voleu donar punts" aria-label="Recipient's username" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button id="botDefinirGrup" class="btn btn-primary" type="button green">Definir</button>
        </div>
      </div>
      <div class="col-12">
        <p class="msgAssignacio">Es donaran els punts a: <span id="grupAssignat"></span></p>
      </div>

      <div class="container">
        <div class="row">
          <div class="text-center col-12">
            <br />
          </div>
          <div class="text-center col-12">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <label id="laboption1" class="btn btn-primary active">
                <input type="radio" name="punts" id="option1" onclick="toggleButton('option1')" autocomplete="off" value="5" checked> 5 coin
              </label>
              <label id="laboption2" class="btn btn-primary">
                <input type="radio" name="punts" id="option2" onclick="toggleButton('option2')" autocomplete="off" value="10"> 10 coin
              </label>
              <label id="laboption3" class="btn btn-primary">
                <input type="radio" name="punts" id="option3" onclick="toggleButton('option3')" autocomplete="off" value="15"> 15 coin
              </label>
            </div>
            <br /><br />
            <button id="botoAssignarPunts" class="btn btn-md btn-primary btn-block" type="submit">Assignar</button>
          </div>

          <div class="my-3 p-3 bg-white rounded box-shadow puntsBox">
            <h6 class="border-bottom border-gray pb-2 mb-0"><?php echo $totals['monstre']; ?> punts assignats - <?php echo $totals['total']; ?> entre tots</h6>
            <?php foreach(getMonstreAssignacions(substr($_SESSION['moniUser'], 0, 1)) as $assignacio){ ?>
            <div class="media text-muted pt-3">
              <div class="assBox"><?php echo $assignacio['quantitat'] ?></div>
              <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <div class="d-flex justify-content-between align-items-center w-100">
                  <strong class="text-gray-dark"><?php if($assignacio['nom']!=""){echo $assignacio['nom'];}else{echo "No nom visible";} ?></strong>
                  <a href="./api.php?accio=desferAssignacio&monstreId=<?php echo substr($_SESSION['moniUser'], 0, 1) ?>&id=<?php echo $assignacio['id'] ?>">Desfer</a>
                </div>
                <span class="d-block">id. <?php echo $assignacio['usuari'] ?></span>
              </div>
            </div>
            <?php } ?>
          </div>
          <div class="text-center col-12">
            <br/>
            <a class="smolTxt" href="./api.php?accio=moniSortir">Sortir del compte</a> <span class="smolTxt">- T'obligarà a tornar-te a identificar</span>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>

</body>

<?php include_once("./layout/modal.html") ?>

<script type="text/javascript" src="./dist/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="./custom.js"></script>

</html>