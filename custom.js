$( "#botoEntrar" ).click(function(e) {
    e.preventDefault();
    if( $("#inputEmail").val() != ""){
      $.ajax({
        type: 'GET',
        url: "./api.php",
        data: {
            accio: 'exist',
            usuari: $("#inputEmail").val(),
        },
        success: function (msg) {
            msg = JSON.parse(msg);
            if (msg.STATUS == "OK") {
                window.location.reload(true);
            } else {
              obrirModal(msg.STATUS);
            }
        }
    });
  } else{
    obrirModal("Introdueix un usuari en el camp.");
  }
});

$( "#definirNom" ).click(function(e) {
  e.preventDefault();
  if( $("#nouNom").val() != ""){
    $.ajax({
      type: 'GET',
      url: "./api.php",
      data: {
          accio: 'canviNom',
          usuari: $("#teamId").html(),
          nouNom: $("#nouNom").val()
      },
      success: function (msg) {
          msg = JSON.parse(msg);
          if (msg.STATUS == "OK") {
              window.location.reload(true);
          } else {
            obrirModal(msg.STATUS);
          }
      }
    });
  } else{
    obrirModal("Escriu primer el nou nom a definir.");
  }
});

function renovarDadesUsuari(){
  $.ajax({
    type: 'GET',
    url: "./api.php",
    data: {
        accio: 'renovarDades',
        usuari: $("#teamId").html()
    },
    success: function (msg) {
      msg = JSON.parse(msg);
      if (msg.STATUS == "OK") {
        $("#puntsGrup").html(msg.INFO.punts);
        $("#puntsTotal").html(msg.INFO.total);
        [1,2,3,4].forEach(function(coso){
          if (msg.MONST.indexOf(coso) >= 0) {
            $(".mons"+coso).css("background-image","url('./img/cross.svg')");
          }else{
            $(".mons"+coso).css("background-image","");
          }
        });
      } else {
        obrirModal(msg.STATUS);
      }
    }
   });
}

//MONITORS!

$( "#botoEntrarMonitors" ).click(function(e) {
    e.preventDefault();
    if( $("#inputEmail").val() != ""){
      $.ajax({
        type: 'GET',
        url: "./api.php",
        data: {
            accio: 'moniExist',
            usuari: $("#inputEmail").val(),
        },
        success: function (msg) {
            msg = JSON.parse(msg);
            if (msg.STATUS == "OK") {
                window.location.reload(true);
            } else {
              obrirModal(msg.STATUS);
            }
        }
    });
  } else{
    obrirModal("Introdueix un usuari en el camp.");
  }
});

$( "#botDefinirGrup" ).click(function(e) {
    e.preventDefault();
    if( $("#grupDefinir").val() != ""){
      $.ajax({
        type: 'GET',
        url: "./api.php",
        data: {
            accio: 'exist',
            usuari: $("#grupDefinir").val(),
        },
        success: function (msg) {
            msg = JSON.parse(msg);
            if (msg.STATUS == "OK") {
                $("#grupAssignat").html($("#grupDefinir").val());
                $(".msgAssignacio").show();
            } else {
              obrirModal(msg.STATUS);
            }
        }
    });
  } else{
    obrirModal("Introdueix un usuari en el camp.");
  }
});

$( "#botoAssignarPunts" ).click(function(e) {
    e.preventDefault();
    if( $("#grupAssignat").html() != ""){
      $.ajax({
        type: 'GET',
        url: "./api.php",
        data: {
            accio: 'assignarPunts',
            usuari: $("#grupDefinir").val(),
            monstreId: $("#moniGrup").html(),
            quantitat: $("input[name='punts']:checked").val()
        },
        success: function (msg) {
            msg = JSON.parse(msg);
            if (msg.STATUS == "OK") {
              window.location.reload(true);
            } else {
              obrirModal(msg.STATUS);
            }
        }
    });
  } else{
    obrirModal("Introdueix un usuari per assignar punts.");
  }
});

function getRanking(){
  $.ajax({
    type: 'GET',
    url: "./api.php",
    data: {
        accio: 'getRanking'
    },
    success: function (msg) {
      $("#marcadorRes").html(msg);
      $(".marcadorPunts").html($("#puntsTotal").val());
    }
   });
}

function activarButSel(id){
    $("#"+id).button('toggle')

}

function obrirModal(modalText){
  $("#modal-text").html(modalText);
  $("#modal-holder").fadeIn(200);
}

function tancarModal(){
  $("#modal-holder").fadeOut(200);
}

function toggleButton(buttonId){
  [1,2,3].forEach(function(c){
    $("#laboption"+c).removeClass("active");
    $("#option"+c).removeAttr("checked");
  });
  $("#lab"+buttonId).addClass("active");
  $("#"+buttonId).attr("checked", "checked");
}