<?php

  include_once("./config.php");

  if(isset($_SESSION['user'])){
    generateConnection();
    $userInfo=getUser($_SESSION['user']);
    $nom=$userInfo[0]['nom'];
    $punts=$userInfo[0]['punts'];
    $puntsTotals=$userInfo[0]['total'];
    $monstros=getUserMonstres($_SESSION['user']);
  }
?>


<!doctype html>
<html lang="ca">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="./favicon.ico">

  <title>Gimcana de Reis- Grup d'Esplai Apassomi</title>

  <link rel="canonical" href="https://apassomi.org/gimcana/">

  <!-- Bootstrap core CSS -->
  <link href="./dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="./custom.css" rel="stylesheet">
</head>

<body>
  <?php if(!isset($_SESSION['user'])){ ?>
  <form id="pag0" class="form-signin">
    <div class="text-center mb-4">
      <h1 class="h3 mb-3 font-weight-normal">Gimcana de Reis</h1>
      <p>Grup d'Esplai Apassomi</p>
    </div>

    <div class="form-label-group">
      <input type="text" id="inputEmail" class="form-control" placeholder="Id. d'Entrada" required autofocus>
      <label for="inputEmail">Id. d'Entrada</label>
    </div>
    <button id="botoEntrar" class="btn btn-md btn-primary btn-block" type="submit">Entrar</button>
    <p class="mt-5 mb-3 text-muted text-center">Grup d'Esplai Apassomi</p>
  </form>
  <?php } else{ ?>
    <div id="pag1" class="form-signin">
      <div class="text-center mb-4">
        <h1 class="h3 mb-3 font-weight-normal user-title">Hola equip, </br><span><?php if(isset($nom) && $nom != ""){ echo $nom; }else{ echo "posa't un nom";} ?></span></h1>
        <p class="user-identifier">Id.de l'equip: <span id="teamId"><?php echo strtoupper($_SESSION['user']) ?></span></p>
      </div>

      <div class="input-group mb-4">
        <input type="text" id="nouNom" class="form-control" placeholder="<?php if(isset($nom) && $nom != ""){ echo $nom; }else{ echo "Nom del grup";} ?>" aria-label="Recipient's username" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button id="definirNom" class="btn btn-primary" type="button green">Definir</button>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="text-center col-12">
            <br/>
          </div>

          <div class="text-center col-6">
            <p>Heu aconseguit:</p>
            <span id="puntsGrup" class="puntsMarcador"><?php if(isset($punts)){ echo $punts; }else{ echo 0; } ?></span>
            <span class="torrocoin">Torrocoins</span>
          </div>
          <div class="text-center col-6">
            <p>Entre tots:</p>
            <span id="puntsTotal" class="puntsMarcador"><?php if(isset($punts)){ echo $puntsTotals; }else{ echo 0; } ?></span>
            <span class="torrocoin">Torrocoins</span>
          </div>

          <div class="text-center col-12">
            <br/><br/>
            Personatges a derrotar:<br/><br/>
            <div class="monsterImg"><div class="crossHolder mons1" <?php if(in_array(1,$monstros)){ echo "style=\"background-image: url('./img/cross.svg');\"";} ?>></div><img alt="M1" src="./img/ginM1.png"/></div>
            <div class="monsterImg"><div class="crossHolder mons2" <?php if(in_array(2,$monstros)){ echo "style=\"background-image: url('./img/cross.svg');\"";} ?>></div><img alt="M2" src="./img/ginM2.png"/></div>
            <div class="monsterImg"><div class="crossHolder mons3" <?php if(in_array(3,$monstros)){ echo "style=\"background-image: url('./img/cross.svg');\"";} ?>></div><img alt="M3" src="./img/ginM3.png"/></div>
            <div class="monsterImg"><div class="crossHolder mons4" <?php if(in_array(4,$monstros)){ echo "style=\"background-image: url('./img/cross.svg');\"";} ?>></div><img alt="M4" src="./img/ginM4.png"/></div>
          </div>
          <div class="text-center col-12">
            <br/>
            <a class="smolTxt" href="./api.php?accio=sortir">Sortir del compte</a> <span class="smolTxt">- T'obligarà a tornar-te a identificar</span>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>

</body>

<?php include_once("./layout/modal.html") ?>

<script type="text/javascript" src="./dist/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="./custom.js"></script>
<script>
var intervalId = setInterval(function() {
  renovarDadesUsuari();
}, 20000);

</script>
</html>