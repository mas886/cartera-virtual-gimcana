<?php 

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

include_once("./config.php");
generateConnection();

$action = $_GET['accio'];

if($_GET['accio'] == 'exist' && isset($_GET['usuari'])){
    $username=strtolower(urldecode($_GET['usuari']));
    if(count(getQuery("SELECT `id`, `usuari`, `nom` FROM `participants` WHERE `usuari` = :usuari",[":usuari"=>$username]))>0){
        $_SESSION['user']=$username;
        echo json_encode(['STATUS'=>"OK"]);
    }else{
        echo json_encode(['STATUS'=>"Aquest identificador és invàlid."]);
    }
}else if($_GET['accio'] == 'canviNom' && isset($_GET['usuari']) && isset($_GET['nouNom'])){
    $usuari=strtolower(urldecode($_GET['usuari']));
    $nom=urldecode($_GET['nouNom']);
    if(executeQuery("UPDATE `participants` SET `nom`=:nom WHERE `usuari`=:usuari",[":usuari"=>$usuari,":nom"=>$nom])){
        echo json_encode(['STATUS'=>"OK"]);
    }else{
        echo json_encode(['STATUS'=>"Error incorrecte."]);
    }
}else if($_GET['accio'] == 'renovarDades' && isset($_GET['usuari'])){
    $usuari=strtolower(urldecode($_GET['usuari']));
    if(count(getQuery("SELECT `id`, `usuari`, `nom` FROM `participants` WHERE `usuari` = :usuari",[":usuari"=>$usuari]))>0){
        echo json_encode(['STATUS'=>"OK",'INFO'=>getUser($_GET['usuari'])[0], 'MONST'=>getUserMonstres($_GET['usuari'])]);
    }else{
        echo json_encode(['STATUS'=>"Error incorrecte."]);
    }
}else if($_GET['accio'] == 'sortir'){
    unset($_SESSION['user']);
    header("Location: ./");
    die();
}else if($_GET['accio'] == 'moniExist' && isset($_GET['usuari'])){
    $monstres=['1MONSTKRAMP','2MONSTOLEN','3MONSTBEFA','4MONSTBELS'];
    $username=urldecode($_GET['usuari']);
    if(in_array($username,$monstres)){
        $_SESSION['moniUser']=$username;
        echo json_encode(['STATUS'=>"OK"]);
    }else{
        echo json_encode(['STATUS'=>"Aquest identificador és invàlid."]);
    }
}else if($_GET['accio'] == 'assignarPunts' && isset($_GET['usuari']) && isset($_GET['monstreId']) && isset($_GET['quantitat'])){
    $usuari=strtolower(urldecode($_GET['usuari']));
    $monstreId=substr(urldecode($_GET['monstreId']), 0, 1);
    $quantitat=$_GET['quantitat'];
    if(executeQuery("INSERT INTO `torrons`(`monstreId`, `participantId`, `quantitat`) VALUES (:monstreId, (SELECT id FROM `participants` WHERE `usuari`=:usuari),:quantitat)",[":monstreId"=>$monstreId,":usuari"=>$usuari,":quantitat"=>$quantitat])){
        echo json_encode(['STATUS'=>"OK"]);
    }else{
        echo json_encode(['STATUS'=>"Error incorrecte."]);
    }
}else if($_GET['accio'] == 'desferAssignacio' && isset($_GET['monstreId']) && isset($_GET['id'])){
    if(executeQuery("DELETE FROM `torrons` WHERE `id`=:id AND `monstreId`=:monstreId",[":monstreId"=>$_GET['monstreId'],":id"=>$_GET['id']])){
        header("Location: ./monitors.php");
        die();
    }else{
        echo json_encode(['STATUS'=>"Error incorrecte."]);
    }
}else if($_GET['accio'] == 'moniSortir'){
    unset($_SESSION['moniUser']);
    header("Location: ./monitors.php");
    die();
}else if($_GET['accio'] == 'getRanking'){
    $usuaris=getQuery("SELECT (SELECT `usuari` FROM `participants` WHERE `id` = `torrons`.`participantId`) usuari, (SELECT `nom` FROM `participants` WHERE `id` = `torrons`.`participantId`) nom, SUM(`quantitat`) total FROM `torrons` WHERE 1 GROUP BY `participantId` ORDER BY `total` DESC");
    $sumaT=0;
    foreach($usuaris as $assignacio){ 
        $sumaT+=$assignacio['total'];
        ?>
        <div class="media text-muted pt-3">
          <div class="assBox"><?php echo $assignacio['total'] ?></div>
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark"><?php if($assignacio['nom']!=""){echo $assignacio['nom'];}else{echo "No nom visible";} ?></strong>
            </div>
            <span class="d-block"></span>
          </div>
        </div>
        <?php } ?> 
        <input type="hidden" id="puntsTotal" value="<?php echo $sumaT; ?>"/>
<?php }else{
    echo "gobac";
}