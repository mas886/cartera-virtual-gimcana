-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Temps de generació: 05-01-2022 a les 01:37:48
-- Versió del servidor: 10.4.22-MariaDB
-- Versió de PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `gimcana`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `participants`
--

CREATE TABLE `participants` (
  `id` int(11) NOT NULL,
  `usuari` varchar(11) NOT NULL,
  `nom` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Bolcament de dades per a la taula `participants`
--

INSERT INTO `participants` (`id`, `usuari`, `nom`) VALUES
(1, '3iow5', ''),
(2, 'ls29w', ''),
(3, 'eh0x1', ''),
(4, 'c4sg1', ''),
(5, 'hzq3y', ''),
(6, 'pxzfo', ''),
(7, 'd3jum', ''),
(8, '8k5wq', ''),
(9, '6y8ij', ''),
(10, 'wwnv1', ''),
(11, '6fpzc', ''),
(12, 'aj904', ''),
(13, '6ok2l', ''),
(14, '8hiaq', ''),
(15, 'epfld', ''),
(16, 'xeszc', ''),
(17, 'htqcq', ''),
(18, 'ecs0o', ''),
(19, 'vyb1c', ''),
(20, '95zwu', ''),
(21, 'h5egy', ''),
(22, '7560y', ''),
(23, '19gb2', ''),
(24, 'mssk2', ''),
(25, 's39xd', ''),
(26, 'nutv8', ''),
(27, 'uw8ow', ''),
(28, 'nu5ud', ''),
(29, 'lg082', ''),
(30, '09fdz', ''),
(31, 'w5ore', ''),
(32, 'ja7bc', ''),
(33, '5xalq', ''),
(34, '0kpem', ''),
(35, 'nq2qv', ''),
(36, 'efwl5', ''),
(37, 'bxtvs', ''),
(38, '8y3an', ''),
(39, '5gsoo', ''),
(40, '6h8ks', ''),
(41, 'pxcg6', ''),
(42, 'djt6q', ''),
(43, '9cibz', ''),
(44, 'vg5s9', ''),
(45, 'gicfd', ''),
(46, 'fqmym', ''),
(47, '9rsg5', ''),
(48, 'bokhs', ''),
(49, '8f8rf', ''),
(50, 'dbjtx', ''),
(51, '8crjt', ''),
(52, 'vjka2', ''),
(53, 'gxzuk', ''),
(54, 'rqys4', ''),
(55, '6271n', ''),
(56, 'oybbn', ''),
(57, 'zvj0k', ''),
(58, '4277v', ''),
(59, 'ys9o4', ''),
(60, 'lvqb2', ''),
(61, 'hglbu', ''),
(62, '9tsgx', ''),
(63, '21j78', ''),
(64, 'afsaq', ''),
(65, 'vtv2u', ''),
(66, 'rsrtz', ''),
(67, '45ldw', ''),
(68, 'q7to6', ''),
(69, '30h2l', ''),
(70, 'vr9n5', ''),
(71, '24d7n', ''),
(72, 'kxm66', ''),
(73, '25bkj', ''),
(74, 'wwhum', ''),
(75, 'ah8g5', ''),
(76, '3q230', ''),
(77, 'xm4my', ''),
(78, 'mhr2l', ''),
(79, 'p51ic', ''),
(80, '0y64z', ''),
(81, 'dwsng', ''),
(82, '6fdxt', ''),
(83, 'utglc', ''),
(84, 'dkni1', ''),
(85, 'g56n6', ''),
(86, '989az', ''),
(87, 'ssks4', ''),
(88, 'livf0', ''),
(89, 'a56xz', ''),
(90, '1ifzg', ''),
(91, 'xcbsq', ''),
(92, 'a8mv6', ''),
(93, 'jcxwb', ''),
(94, '7j0l5', ''),
(95, 'ksvto', ''),
(96, '52784', ''),
(97, 'dfnb3', ''),
(98, '8693b', ''),
(99, '7c96y', ''),
(100, '5vc7w', ''),
(101, 'kl5f4', ''),
(102, '4tfwy', ''),
(103, 's99hi', '');

-- --------------------------------------------------------

--
-- Estructura de la taula `torrons`
--

CREATE TABLE `torrons` (
  `id` int(11) NOT NULL,
  `monstreId` int(11) NOT NULL,
  `participantId` int(11) NOT NULL,
  `quantitat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Índexs per a la taula `torrons`
--
ALTER TABLE `torrons`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT per la taula `torrons`
--
ALTER TABLE `torrons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
